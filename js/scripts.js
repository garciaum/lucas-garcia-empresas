var modulo = angular.module('infoEmpresas', []);

modulo.controller('buscaController', ['$scope', '$http', '$templateCache', '$window', '$filter',
    function($scope, $http, $templateCache, $window, $filter) {

        $scope.header = { 'Content-Type': 'application/json' };
        $scope.pagina = "login";
        $scope.credencial = { email: "" };
        $scope.credencial = { senha: "" };
        $scope.dadosPesquisa = { text: "" };
        $scope.investor = {};
        $scope.searchEnd = false;
        $scope.searchEmpty = false;
        $scope.selectedEnterprise = undefined;
        $scope.showClearIcon = false;

        $scope.login = function() {

            $http({
                method: 'POST',
                url: 'http://54.94.179.135:8090/api/v1/users/auth/sign_in',
                headers: $scope.header,
                data: {
                    "email": $scope.credencial.email,
                    "password": $scope.credencial.senha
                }
            }).then(function successCallback(response) {

                $scope.investor = response.data.investor;

                $scope.header['client'] = response.headers().client;
                $scope.header['access-token'] = response.headers()['access-token'];
                $scope.header['uid'] = response.headers().uid;

                $scope.abrirPesquisa();

            }, function errorCallback(response) {
                $scope.erro = { autenticacao: "Usuário ou senha incorreto!" };
            });
        };

        $scope.pesquisa = function() {

            if ($scope.dadosPesquisa.text != "" && $scope.dadosPesquisa.text != undefined)
                return true;

            return false;
        };

        $scope.abrirPesquisa = function() {

            $scope.pagina = "paginaPesquisa";
            $scope.enterprises = [];

            $http({
                method: 'GET',
                url: 'http://54.94.179.135:8090/api/v1/enterprises',
                headers: $scope.header
            }).then(function successCallback(response) {

                if (response.status != 200) {
                    $scope.exibeErro(response);
                    return;
                }

                $scope.enterprises = response.data.enterprises;

            }, function errorCallback(response) {
                $scope.exibeErro(response);
            });
        };

        $scope.detalhes = function(id) {

            $scope.pagina = "paginaDetalhes";
            $scope.limpaPesquisa();

            $http({
                method: 'GET',
                url: "http://54.94.179.135:8090/api/v1/enterprises/" + id,
                headers: $scope.header
            }).then(function successCallback(response) {

                if (response.status != 200) {
                    $scope.exibeErro(response);
                    return;
                }

                $scope.selectedEnterprise = response.data.enterprise;

            }, function errorCallback(response) {
                $scope.exibeErro(response);
            });
        };

        $scope.expandePesquisa = function() {
            var element = $window.document.getElementById("textoPesquisa");
            element.focus();
        };

        $scope.limpaPesquisa = function() {
            $scope.dadosPesquisa = { text: "" };
        };

        $scope.voltar = function() {
            $scope.selectedEnterprise = {};
            $scope.pagina = "paginaPesquisa";
        };


        $scope.exibeErro = function(response) {
            console.log("Erro na requisição");
            console.log(response.status);
            console.log(response.data);
        };

    }
]);

modulo.filter('enterpriseFilter', function() {

    return function(enterprises, text) {

        var checkItem = function(item) {
            if (item.enterprise_name.toLowerCase().match(text.toLowerCase()))
                return true;

            if (item.enterprise_type.enterprise_type_name.toLowerCase().match(text.toLowerCase()))
                return true;

            return false;
        };

        var out = [];
        out = enterprises.filter(checkItem);

        return out;
    }
});